; Script generated by the Inno Script Studio Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "Talisman BBS"
#define MyAppVersion "0.53"
#define MyAppPublisher "Andrew Pamment"
#define MyAppURL "https://talismanbbs.com/"
#define MyAppExeName "Servo.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{7B0968D1-22AF-4B04-B3DF-8352C74331D4}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName=C:\bbs\
DefaultGroupName={#MyAppName}
OutputBaseFilename=Talisman-v{#MyAppVersion}-Setup
Compression=lzma
SolidCompression=yes
LicenseFile=..\..\LICENSE
WizardSizePercent=120,140
[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\Users\apamm\src\Talisman\Release\Servo.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Release\Talisman.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Release\Toolbelt.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Release\Postie.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Release\Gofer.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Release\Falcon.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Release\Qwkie.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Release\Binki.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\system.ans"; DestDir: "{app}\gfiles\"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\system.asc"; DestDir: "{app}\gfiles\"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\gfiles\*"; DestDir: "{app}\gfiles\"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\gfiles\pause\*"; DestDir: "{app}\gfiles\pause\"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\menus\*"; DestDir: "{app}\menus\"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\scripts\*"; DestDir: "{app}\scripts"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\gophermap"; DestDir: "{app}\gopher"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\trashcan.txt"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\bulletins.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\msgconfs.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\mb_local.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\fileconfs.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\fb_general.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\seclevels.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\loginitems.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\data\postie.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\*.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\*.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\protocols.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\archivers.toml"; DestDir: "{app}\data"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\curl-8.5.0\build\Win32\VC14.30\DLL Release - DLL OpenSSL\libcurl.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Program Files (x86)\OpenSSL-Win32\bin\libcrypto-3.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Program Files (x86)\OpenSSL-Win32\bin\libssl-3.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\talisman.ini"; DestDir: "{app}"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\talisman.ini"; DestDir: "{app}\dist"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\gfiles\*"; DestDir: "{app}\dist\gfiles\"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\gfiles\pause\*"; DestDir: "{app}\dist\gfiles\pause\"; Flags: onlyifdoesntexist
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\system.ans"; DestDir: "{app}\dist\gfiles\"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\system.asc"; DestDir: "{app}\dist\gfiles\"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\menus\*"; DestDir: "{app}\dist\menus\"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\scripts\*"; DestDir: "{app}\dist\scripts"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\gophermap"; DestDir: "{app}\dist\gopher"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\trashcan.txt"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\bulletins.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\msgconfs.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\mb_local.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\fileconfs.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\fb_general.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\seclevels.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\loginitems.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\data\postie.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\protocols.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
Source: "C:\Users\apamm\src\Talisman\Talisman\win32_deps\archivers.toml"; DestDir: "{app}\dist\data"; Flags: ignoreversion
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Dirs]
Name: "{app}\data"
Name: "{app}\temp"
Name: "{app}\logs"
Name: "{app}\gfiles"
Name: "{app}\gfiles\pause"
Name: "{app}\menus"
Name: "{app}\msgs"
Name: "{app}\scripts\"
Name: "{app}\scripts\data"
Name: "{app}\gopher"
Name: "{app}\dloads"
Name: "{app}\dloads\general"
Name: "{app}\dloads\general\uploads"
Name: "{app}\dloads\general\misc"
Name: "{app}\dist"
Name: "{app}\dist\gfiles"
Name: "{app}\dist\gfiles\pause"
Name: "{app}\dist\data"
Name: "{app}\dist\menus"
Name: "{app}\dist\scripts"
Name: "{app}\dist\gopher"

[Icons]
Name: "{group}\{#MyAppName} (Servo)"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{#MyAppName} (Documentation)"; Filename: "https://talismanbbs.com/docs/"
Name: "{commondesktop}\{#MyAppName} (Servo)"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{commondesktop}\{#MyAppName} (Documentation)"; Filename: "https://talismanbbs.com/docs/"; Tasks: desktopicon

[Code]
var
  BBSDetailsPage: TInputQueryWizardPage;
  BBSName : string;
  SysopName : string;
  LocationStr : string;
  QwkIdStr : string;
  HostnameStr : string;
  InstallPath : string;
  UpgradeBool : Boolean;
function FileReplaceString(const FileName, SearchString, ReplaceString: string):boolean;
var
  MyFile : TStrings;
  MyText : string;
begin
  MyFile := TStringList.Create;

  try
    result := true;

    try
      MyFile.LoadFromFile(FileName);
      MyText := MyFile.Text;

      { Only save if text has been changed. }
      if StringChangeEx(MyText, SearchString, ReplaceString, True) > 0 then
      begin;
        MyFile.Text := MyText;
        MyFile.SaveToFile(FileName);
      end;
    except
      result := false;
    end;
  finally
    MyFile.Free;
  end;
end;

function GetUninstallString: string;
var
  sUnInstPath: string;
  sUnInstallString: String;
begin
  Result := '';
  sUnInstPath := ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\{{7B0968D1-22AF-4B04-B3DF-8352C74331D4}_is1'); { Your App GUID/ID }
  sUnInstallString := '';
  if not RegQueryStringValue(HKLM, sUnInstPath, 'UninstallString', sUnInstallString) then
    RegQueryStringValue(HKCU, sUnInstPath, 'UninstallString', sUnInstallString);
  Result := sUnInstallString;
end;

function IsUpgrade: Boolean;
begin
  Result := (GetUninstallString() <> '');
end;

procedure InitializeWizard;
begin
  if IsUpgrade() = false then
  begin;
    BBSDetailsPage := CreateInputQueryPage(wpWelcome,
      'BBS Details', 'Tell me about your BBS...',
      'Please specify your BBS Details, ' +
      'then click Next.');
    BBSDetailsPage.Add('BBS Name', false);
    BBSDetailsPage.Add('Sysop Username', false);
    BBSDetailsPage.Add('Location', false);
    BBSDetailsPage.Add('QWK ID', false);
    BBSDetailsPage.Add('BBS Hostname', false);
    UpgradeBool := false;
  end else
  begin;
    UpgradeBool := true;
  end;
end;
procedure CurStepChanged(CurStep: TSetupStep);
begin
  if CurStep = ssPostInstall then
  begin;
    if UpgradeBool = false then
    begin;
      if (Length(BBSDetailsPage.Values[0]) > 0) then
        BBSName := BBSDetailsPage.Values[0]
      else
        BBSName := 'Talisman BBS';

      if (Length(BBSDetailsPage.Values[1]) > 0) then
        SysopName := BBSDetailsPage.Values[1]
      else
        SysopName := 'Sysop';

      if (Length(BBSDetailsPage.Values[2]) > 0) then
        LocationStr := BBSDetailsPage.Values[2]
      else
        LocationStr := 'Somewhere, The World';

      if (Length(BBSDetailsPage.Values[3]) > 0) then
        QwkIdStr := Copy(Uppercase(BBSDetailsPage.Values[3]), 0, 8)
      else
        QwkIdStr := 'TALISMAN';
      if (Length(BBSDetailsPage.Values[4]) > 0) then
        HostnameStr := BBSDetailsPage.Values[4]
      else
        HostnameStr := 'localhost';

      InstallPath := ExpandConstant('{app}');

      StringChangeEx(InstallPath, '\', '\\', True)

      FileReplaceString(ExpandConstant('{app}\talisman.ini'), '__BBS_NAME__', BBSName);
      FileReplaceString(ExpandConstant('{app}\talisman.ini'), '__SYSOP_NAME__', SysopName);
      FileReplaceString(ExpandConstant('{app}\talisman.ini'), '__LOCATION__', LocationStr);
      FileReplaceString(ExpandConstant('{app}\talisman.ini'), '__QWK_ID__', QwkIdStr);
      FileReplaceString(ExpandConstant('{app}\talisman.ini'), '__HOSTNAME__', HostnameStr);
      FileReplaceString(ExpandConstant('{app}\data\protocols.toml'), '__INST_PATH__', InstallPath);
      FileReplaceString(ExpandConstant('{app}\data\archivers.toml'), '__INST_PATH__', InstallPath);
      FileReplaceString(ExpandConstant('{app}\dist\talisman.ini'), '__BBS_NAME__', BBSName);
      FileReplaceString(ExpandConstant('{app}\dist\talisman.ini'), '__SYSOP_NAME__', SysopName);
      FileReplaceString(ExpandConstant('{app}\dist\talisman.ini'), '__LOCATION__', LocationStr);
      FileReplaceString(ExpandConstant('{app}\dist\talisman.ini'), '__QWK_ID__', QwkIdStr);
      FileReplaceString(ExpandConstant('{app}\dist\talisman.ini'), '__HOSTNAME__', HostnameStr);
      FileReplaceString(ExpandConstant('{app}\dist\data\protocols.toml'), '__INST_PATH__', InstallPath);
      FileReplaceString(ExpandConstant('{app}\dist\data\archivers.toml'), '__INST_PATH__', InstallPath);
    end;
  end;
end;
