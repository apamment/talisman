cmake_minimum_required (VERSION 3.0)
project (bridge)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_MODULE_PATH ${bridge_SOURCE_DIR}/../cmake/)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -ggdb")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall")
add_executable(bridge main.cpp bridge.cpp ../Common/Squish.cpp ../Common/Logger.cpp)
include_directories(../Common)
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	target_link_libraries (bridge)
else()
	target_link_libraries (bridge -lstdc++fs)
endif()
