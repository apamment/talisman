cmake_minimum_required (VERSION 3.0)
project (qwkie)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_MODULE_PATH ${qwkie_SOURCE_DIR}/../cmake/)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -ggdb")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall")
find_package(CURL REQUIRED)
add_executable(qwkie main.cpp qwkie.cpp Archiver.cpp ../Common/Squish.cpp ../Common/Logger.cpp)
include_directories(${CURL_INCLUDE_DIRS} ../Common)
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	target_link_libraries (qwkie ${CURL_LIBRARIES})
else()
	if (CMAKE_SYSTEM_NAME MATCHES "Haiku")
		target_link_libraries (qwkie ${CURL_LIBRARIES})
	else()
		target_link_libraries (qwkie ${CURL_LIBRARIES} -lstdc++fs)
	endif()
endif()
